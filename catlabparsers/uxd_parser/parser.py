#
# Copyright The NOMAD Authors.
#
# This file is part of NOMAD. See https://nomad-lab.eu for further info.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from nomad.datamodel import EntryArchive
from nomad.parsing import MatchingParser

import os
from datetime import datetime


'''
This is a hello world style example for an example parser/converter.
'''


class UXDParser(MatchingParser):
    def __init__(self):
        super().__init__(
            name='parsers/uxdparsercatlab',
            code_name='uxdparsercatlab',
            code_homepage='https://www.example.eu/',
            supported_compressions=[
                'gz',
                'bz2',
                'xz'])

    def parse(self, mainfile: str, archive: EntryArchive, logger):
        # Log a hello world, just to get us started. TODO remove from an actual
        # parser.
        archive.metadata.entry_name = os.path.basename(mainfile)        

        measurement_base, measurement_name = os.path.split(mainfile)
        mainfile_split = measurement_name.split("_")
        date = mainfile_split[0]
        sample_id = mainfile_split[1]
        method = mainfile_split[2]

        file_beginning = "_".join([date, sample_id, method])
        files = [file for file in os.listdir(
            measurement_base) if file.startswith(file_beginning)]
        
        entry = None
        if method.lower().startswith("xrr"):
            from nomad.datamodel.metainfo.eln.application_catlab import CatLab_FHI_IRIS_XRR_Brucker
            from nomad.datamodel.metainfo.eln.helper.fhi_archive import get_xrr_data_entry
            measurement, fitted_data = get_xrr_data_entry(archive, files)
            entry = CatLab_FHI_IRIS_XRR_Brucker(
                measurement=measurement,
                fitted_data=fitted_data,
                data_file=files
            )

        if method.lower().startswith("xrd"):
            from nomad.datamodel.metainfo.eln.application_catlab import CatLab_FHI_IRIS_XRD_Brucker
            from nomad.datamodel.metainfo.eln.helper.fhi_archive import get_xrd_data_entry
            measurements, shifted_data = get_xrd_data_entry(archive, files)
            entry = CatLab_FHI_IRIS_XRD_Brucker(
                measurements=measurements,
                shifted_data=shifted_data,
                data_file=files
            )

        if entry is None:
            return
        
        if "comment.txt" in os.listdir(measurement_base):
            with archive.m_context.raw_file("comment.txt", "r") as f:
                entry.description = f.read()
                
        datetime_str = date
        datetime_object = datetime.strptime(
            datetime_str, '%y%m%d')
        entry.datetime = datetime_object.strftime(
            "%Y-%m-%d %H:%M:%S.%f")
        
        entry.name = file_beginning.replace("_", " ")
        from nomad.datamodel.metainfo.eln.helper.utilities import create_archive, find_sample_by_id

        entry.samples = [find_sample_by_id(archive, sample_id)]

        file_name = f'{file_beginning}.archive.json'
        create_archive(entry, archive, file_name)
